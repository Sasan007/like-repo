﻿using Like.Context.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Like.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        LikeDbContext Context { get; }
        void Commit();
    }
}
