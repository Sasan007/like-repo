﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Like.Application.Contract
{
    public interface ILikeHistoryService
    {
        bool IsExistArticleLikedByIp(int articleId, string ip);
        void RegisterIpForArticle(int articleId,string ip);
        void UnregisterIpForArticle(int articleId,string ip);
    }
}
