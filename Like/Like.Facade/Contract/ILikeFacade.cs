﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Like.Facade.Contract
{
    public interface ILikeFacade
    {
        int GetLatestArticleLike(int articleId);
        int UpdateArticleLike(int articleId, string ip);
    }
}
