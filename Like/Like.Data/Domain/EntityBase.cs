﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Like.Data.Domain
{
    public class EntityBase
    {
        public long Id { get; set; }
        [Timestamp] public byte[] RowVersion { get; set; }
    }
}