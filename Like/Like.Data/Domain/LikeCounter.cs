﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Like.Data.Domain
{
    public class LikeCounter : EntityBase
    {
        public int ArticleId { get; set; }

        public int LikeCount { get; set; }
    }
}
