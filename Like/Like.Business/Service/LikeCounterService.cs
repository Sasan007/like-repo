﻿using Like.Application.Contract;
using Like.Data.Domain;
using Like.Data.Repository;
using Like.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Like.Application.Service
{
    public class LikeCounterService : ILikeCounterService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<LikeCounter> _likeCounterRepository;

        public LikeCounterService(IUnitOfWork unitOfWork, IRepository<LikeCounter> likeCounterRepository)
        {
            _unitOfWork = unitOfWork;
            _likeCounterRepository = likeCounterRepository;
        }

        public int GetLikeCount(int articleId)
        {
            var likeCounter = _likeCounterRepository.Get(q => q.ArticleId == articleId).FirstOrDefault();
            if (likeCounter != null)
                return likeCounter.LikeCount;
            return 0;
        }

        public int IncrementLike(int articleId, string ip)
        {
            LikeCounter likeCounter = _likeCounterRepository.Get(q => q.ArticleId == articleId).FirstOrDefault();
            likeCounter.LikeCount++;

            _likeCounterRepository.Update(likeCounter);
            _unitOfWork.Commit();

            return likeCounter.LikeCount;
        }

        public int DecrementLike(int articleId, string ip)
        {
            LikeCounter likeCounter = _likeCounterRepository.Get(q => q.ArticleId == articleId).FirstOrDefault();
            likeCounter.LikeCount--;

            _likeCounterRepository.Update(likeCounter);
            _unitOfWork.Commit();

            return likeCounter.LikeCount;
        }

        public bool HasEverArticleLiked(int articleId)
        {
            return _likeCounterRepository.Get(q => q.ArticleId == articleId).Any();
        }

        public int SeedLike(int articleId, string ip)
        {
            LikeCounter likeCounter = new LikeCounter();
            likeCounter.ArticleId = articleId;
            likeCounter.LikeCount = 1;

            _likeCounterRepository.Update(likeCounter);
            _unitOfWork.Commit();

            return likeCounter.LikeCount;
        }
    }
}