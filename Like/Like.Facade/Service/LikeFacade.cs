﻿using Like.Application.Contract;
using Like.Facade.Contract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Like.Facade.Service
{
    public class LikeFacade : ILikeFacade
    {
        private readonly ILikeCounterService _likeCounterService;
        private readonly ILikeHistoryService _likeHistoryService;

        public LikeFacade(ILikeCounterService likeCounterService,ILikeHistoryService likeHistoryService)
        {
            _likeCounterService = likeCounterService;
            _likeHistoryService = likeHistoryService;
        }
        public int GetLatestArticleLike(int articleId)
        {
            return _likeCounterService.GetLikeCount(articleId);
        }

        public int UpdateArticleLike(int articleId, string ip)
        {
            int result;

            if (!_likeCounterService.HasEverArticleLiked(articleId))
            {
                result = _likeCounterService.SeedLike(articleId, ip);
                _likeHistoryService.RegisterIpForArticle(articleId, ip);
            }
            else
            {
                if (!_likeHistoryService.IsExistArticleLikedByIp(articleId, ip))
                {
                    result = _likeCounterService.IncrementLike(articleId, ip);
                    _likeHistoryService.RegisterIpForArticle(articleId, ip);
                }
                else
                {
                    result = _likeCounterService.DecrementLike(articleId, ip);
                    _likeHistoryService.UnregisterIpForArticle(articleId, ip);
                }
            }
            return result;
        }
    }
}
