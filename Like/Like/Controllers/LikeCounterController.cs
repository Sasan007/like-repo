﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Like.Application.Contract;
using Like.Facade.Contract;
using Like.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Like.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LikeCounterController : ControllerBase
    {
        private readonly ILikeFacade _likeFacade;

        public LikeCounterController(ILikeFacade likeFacade)
        {
            _likeFacade = likeFacade;
        }

        [HttpGet("{articleId}")]
        public ActionResult<int> Get(int articleId)
        {
            return _likeFacade.GetLatestArticleLike(articleId);
        }

        [HttpPut]
        public int UpdateLike(IncrementArticleLikeViewModel incrementArticleLikeViewModel)
        {
            return _likeFacade.UpdateArticleLike(incrementArticleLikeViewModel.ArticleId, incrementArticleLikeViewModel.Ip);
        }
    }
}
