import React from 'react';
import './Like.css';
import axios from 'axios';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp as farThumbsUp } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import publicIP from 'react-native-public-ip';

const apiUrl = 'https://localhost:44391/api/LikeCounter/';
const publicIp = require('public-ip');
let ipAddress;
publicIP()
  .then(ip => {
    ipAddress=ip;
  })
  .catch(error => {
    console.log(error);
  });
  

class LikeButton extends React.Component {

    state = {
        likes: 0
    };


    addLike = () => {
        debugger
        
        axios({
            method: 'put',
            url: apiUrl,
            headers: {},
            data: {
                articleId: 10,
                ip: ipAddress
            }
        }).then((repos) => {
            this.setState({
                likes: repos.data
            });
        });
    };
    getData() {
        console.log(publicIp.v4());
        axios.get(apiUrl + '10').then((repos) => {
            this.setState({
                likes: repos.data
            });
        });
    }

    componentDidMount() {
        this.getData();
    }
    render() {

        const likes = this.state.likes;
        if (likes === 0) {
            return (
                <div>
                    <button
                        className="button"
                        onClick={this.addLike}
                    >
                        <FontAwesomeIcon icon={farThumbsUp} style={{ fontSize: "20px", color: "#33c3f0" }} />
                    </button>
                </div>
            );
        }
        if (likes === 1) {
            return (
                <div>
                    <button className="button" onClick={this.addLike}>
                        <FontAwesomeIcon icon={faThumbsUp} style={{ fontSize: "20px", color: "#33c3f0" }} />
                    </button>
                </div>
            );
        }
        if (likes > 1) {
            return (
                <div>
                    <button className="button" onClick={this.addLike}>
                        <FontAwesomeIcon icon={faThumbsUp} style={{ fontSize: "20px", color: "#33c3f0" }} />{" "}
                        {likes}
                    </button>
                </div>
            );
        }
    }
}
export default LikeButton