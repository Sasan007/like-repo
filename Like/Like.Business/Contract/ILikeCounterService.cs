﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Like.Application.Contract
{
    public interface ILikeCounterService
    {
        int GetLikeCount(int articleId);
        bool HasEverArticleLiked(int articleId);
        int SeedLike(int articleId, string ip);
        int IncrementLike(int articleId, string ip);
        int DecrementLike(int articleId, string ip);
    }
}
