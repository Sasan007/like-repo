﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Like.Context.Data;
using Microsoft.EntityFrameworkCore;

namespace Like.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public LikeDbContext Context { get; }

        public UnitOfWork(LikeDbContext context)
        {
            Context = context;
        }

        public void Commit()
        {
            var strategy = Context.Database.CreateExecutionStrategy();

            strategy.Execute(() =>
            {
                using (var dbContextTransaction = Context.Database.BeginTransaction())
                {
                    try
                    {
                        Context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        ex.Entries.Single().Reload();
                        Context.SaveChanges();
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
            });
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
