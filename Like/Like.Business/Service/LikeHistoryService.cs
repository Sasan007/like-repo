﻿using Like.Application.Contract;
using Like.Data.Domain;
using Like.Data.Repository;
using Like.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Like.Application.Service
{
    public class LikeHistoryService : ILikeHistoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<LikeHistory> _likeHistoryRepository;

        public LikeHistoryService(IUnitOfWork unitOfWork, IRepository<LikeHistory> likeHistoryRepository)
        {
            _unitOfWork = unitOfWork;
            _likeHistoryRepository = likeHistoryRepository;
        }

        public bool IsExistArticleLikedByIp(int articleId, string ip)
        {
            return _likeHistoryRepository.Get(q => q.IP == ip && q.ArticleId == articleId).Any();
        }

        public void RegisterIpForArticle(int articleId, string ip)
        {
            LikeHistory likeHistory = new LikeHistory();

            likeHistory.ArticleId = articleId;
            likeHistory.IP = ip;

            _likeHistoryRepository.Add(likeHistory);
            _unitOfWork.Commit();
        }

        public void UnregisterIpForArticle(int articleId, string ip)
        {
            LikeHistory likeHistory = _likeHistoryRepository.Get(q => q.IP == ip && q.ArticleId == articleId)
                .FirstOrDefault();

            _likeHistoryRepository.Delete(likeHistory);
            _unitOfWork.Commit();
        }
    }
}