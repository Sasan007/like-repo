﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Like.Data.Domain
{
    public class LikeHistory : EntityBase
    {
        public int ArticleId { get; set; }
        public string IP { get; set; }
    }
}