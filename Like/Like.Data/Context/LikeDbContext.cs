﻿using Like.Data.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;

namespace Like.Context.Data
{
    public class LikeDbContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Data Source=.;Initial Catalog=LikeDB;Integrated Security=true",
                sqlServerOptionsAction: sqloptions => { sqloptions.EnableRetryOnFailure(); });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LikeCounter>()
                .Property(p => p.RowVersion)
                .IsRowVersion();

            modelBuilder.Entity<LikeHistory>()
                .Property(p => p.RowVersion)
                .IsRowVersion();
        }
    }
}