﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Like.ViewModel
{
    public class IncrementArticleLikeViewModel
    {
        public int ArticleId { get; set; }
        public string Ip { get; set; }
    }
}
